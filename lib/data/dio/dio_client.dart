import 'package:dio/dio.dart';

class DioClient {
  final String baseUrl;
  DioClient(this.baseUrl)
      : dio = Dio(
    BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 5000,
      receiveTimeout: 3000,
      responseType: ResponseType.json,
    ),
  )
    ..interceptors.add(LogInterceptor(requestBody: true, responseBody: true));

  final Dio dio;

}
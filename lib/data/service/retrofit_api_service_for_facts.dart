import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

import '../../domain/models/fact_model.dart';

part 'retrofit_api_service_for_facts.g.dart';

@RestApi()
abstract class RetrofitApiServiceForFacts {
  factory RetrofitApiServiceForFacts(Dio dio, {String baseUrl}) = _RetrofitApiServiceForFacts;


  @GET('/facts/random')
  Future<FactModel> getFact(
    @Query("animal_type") String animalType,
  );
}

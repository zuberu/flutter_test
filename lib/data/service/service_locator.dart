import 'package:get_it/get_it.dart';
import 'package:testcatsappflutter/data/dio/dio_client.dart';
import 'package:testcatsappflutter/data/service/retrofit_api_service_for_facts.dart';
import 'package:testcatsappflutter/environment.dart';
import 'package:testcatsappflutter/feature/facts/fact_bloc.dart';

class ServiceLocator {
  final GetIt _getIt;

  ServiceLocator() : _getIt = GetIt.asNewInstance();

  RetrofitApiServiceForFacts get retrofitApiServiceForFacts => _getIt.get<RetrofitApiServiceForFacts>();
  FactBloc get factBloc => _getIt.get<FactBloc>();

  void config() {
    _getIt.registerLazySingleton(() => DioClient(Environment.baseUrl));
    _getIt.registerLazySingleton(() => RetrofitApiServiceForFacts(_getIt.get<DioClient>().dio));

    configBLoc();
  }

  void configBLoc() {
    _getIt.registerLazySingleton(() => FactBloc(_getIt.get()));
  }
}
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testcatsappflutter/common/extension/context_extension.dart';
import 'package:testcatsappflutter/environment.dart';

import 'fact_bloc.dart';

class FactScreen extends StatefulWidget {
  const FactScreen({Key? key}) : super(key: key);

  @override
  State<FactScreen> createState() => _FactScreenState();
}

class _FactScreenState extends State<FactScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Facts about cats"),
      ),
      body: BlocBuilder<FactBloc, FactState>(
        bloc: context.factBloc..loadFacts(),
        builder: (context, state) => state.loading
            ? const Center(child: CircularProgressIndicator())
            : Align(
                alignment: Alignment.center,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ConstrainedBox(
                          constraints: const BoxConstraints(maxHeight: 200),
                          child: Image.network(
                            Environment.baseUrlForImage,
                            fit: BoxFit.fill,
                          )),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(state.factModel?.text ?? ""),
                      const SizedBox(
                        height: 20,
                      ),
                      MaterialButton(
                          onPressed: context.factBloc.loadFacts,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: const BorderSide(color: Colors.cyan)),
                          child: const Text("Load More Facts!!"))
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}

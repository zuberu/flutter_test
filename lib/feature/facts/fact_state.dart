part of 'fact_bloc.dart';

@immutable
class FactState extends Equatable {
  final FactModel? factModel;
  final bool loading;
  final String errorMessage;
  final String imageUrl;

  const FactState._(this.factModel, this.loading, this.errorMessage,this.imageUrl);

  const FactState._init() : this._(null, false, "","");

  FactState copyWith(
          {FactModel? factModel, bool? loading, String? errorMessage, String? imageUrl}) =>
      FactState._(factModel ?? this.factModel, loading ?? this.loading,
          errorMessage ?? this.errorMessage, imageUrl?? this.imageUrl);

  @override
  List<Object?> get props => [loading, factModel, errorMessage,imageUrl];
}

import 'dart:async';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testcatsappflutter/data/service/retrofit_api_service_for_facts.dart';
import 'package:testcatsappflutter/domain/models/fact_model.dart';

part 'fact_event.dart';
part 'fact_state.dart';

class FactBloc extends Bloc<FactEvent, FactState> {
  final RetrofitApiServiceForFacts _retrofitApiServiceForFacts;

  FactBloc(this._retrofitApiServiceForFacts)
      : super(const FactState._init()) {
    on<LoadDataEvent>(_loadDataEvent);
  }

  void loadFacts() {
    add(const LoadDataEvent._());
  }

  FutureOr<void> _loadDataEvent(
      LoadDataEvent event, Emitter<FactState> emit) async {
    emit(state.copyWith(loading: true));
    try {
      var result = await _retrofitApiServiceForFacts.getFact("cat");
      emit(state.copyWith(factModel: result, loading: false,));
    } on DioError catch (e) {
      emit(state.copyWith(errorMessage: e.message, loading: false));
    }
  }
}

part of 'fact_bloc.dart';

abstract class FactEvent extends Equatable {
   const FactEvent._();

   @override
   List<Object?> get props => [];
}

@immutable
class LoadDataEvent extends FactEvent {
  const LoadDataEvent._() : super._();
}

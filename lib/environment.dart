
import 'package:flutter/material.dart';

@immutable
class Environment {
    static const baseUrl = String.fromEnvironment('BASE_URL' , defaultValue: "");
    static const baseUrlForImage = String.fromEnvironment('BASE_IMAGE_URL' , defaultValue: "");
}
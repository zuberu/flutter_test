import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testcatsappflutter/data/service/retrofit_api_service_for_facts.dart';
import 'package:testcatsappflutter/data/service/service_locator.dart';
import 'package:testcatsappflutter/feature/facts/fact_bloc.dart';

extension AppGlobalInjectable on BuildContext {
  RetrofitApiServiceForFacts get retrofitApiServiceForFacts =>
      read<ServiceLocator>().retrofitApiServiceForFacts;

  FactBloc get factBloc => read<ServiceLocator>().factBloc;
}

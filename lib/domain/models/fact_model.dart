import 'status.dart';

class FactModel {
  FactModel({
      Status? status, 
      String? id, 
      String? updatedAt, 
      String? createdAt, 
      String? user, 
      String? text, 
      bool? deleted, 
      String? source, 
      int? v, 
      String? type, 
      bool? used,}){
    _status = status;
    _id = id;
    _updatedAt = updatedAt;
    _createdAt = createdAt;
    _user = user;
    _text = text;
    _deleted = deleted;
    _source = source;
    _v = v;
    _type = type;
    _used = used;
}

  FactModel.fromJson(dynamic json) {
    _status = json['status'] != null ? Status.fromJson(json['status']) : null;
    _id = json['_id'];
    _updatedAt = json['updatedAt'];
    _createdAt = json['createdAt'];
    _user = json['user'];
    _text = json['text'];
    _deleted = json['deleted'];
    _source = json['source'];
    _v = json['__v'];
    _type = json['type'];
    _used = json['used'];
  }
  Status? _status;
  String? _id;
  String? _updatedAt;
  String? _createdAt;
  String? _user;
  String? _text;
  bool? _deleted;
  String? _source;
  int? _v;
  String? _type;
  bool? _used;

  Status? get status => _status;
  String? get id => _id;
  String? get updatedAt => _updatedAt;
  String? get createdAt => _createdAt;
  String? get user => _user;
  String? get text => _text;
  bool? get deleted => _deleted;
  String? get source => _source;
  int? get v => _v;
  String? get type => _type;
  bool? get used => _used;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_status != null) {
      map['status'] = _status?.toJson();
    }
    map['_id'] = _id;
    map['updatedAt'] = _updatedAt;
    map['createdAt'] = _createdAt;
    map['user'] = _user;
    map['text'] = _text;
    map['deleted'] = _deleted;
    map['source'] = _source;
    map['__v'] = _v;
    map['type'] = _type;
    map['used'] = _used;
    return map;
  }

}

class Status {
  Status({
      bool? verified, 
      int? sentCount,}){
    _verified = verified;
    _sentCount = sentCount;
}

  Status.fromJson(dynamic json) {
    _verified = json['verified'];
    _sentCount = json['sentCount'];
  }
  bool? _verified;
  int? _sentCount;

  bool? get verified => _verified;
  int? get sentCount => _sentCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['verified'] = _verified;
    map['sentCount'] = _sentCount;
    return map;
  }

}
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testcatsappflutter/common/extension/context_extension.dart';
import 'package:testcatsappflutter/environment.dart';
import 'package:testcatsappflutter/feature/facts/fact_screen.dart';

import 'data/service/service_locator.dart';

void main() {

  WidgetsFlutterBinding.ensureInitialized();
  final ServiceLocator serviceLocator = ServiceLocator()..config();
  runApp(MyApp(serviceLocator));
}

class MyApp extends StatelessWidget {
  final ServiceLocator serviceLocator;
  const MyApp(this.serviceLocator, {Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test Cat App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MultiRepositoryProvider(
          providers: [
            RepositoryProvider.value(value: serviceLocator)
          ],
          child: const FactScreen()),
    );
  }
}